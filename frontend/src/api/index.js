const listQuiz = [
  {
    objectID: 1,
    name: "Technology Quiz",
    content: [
      {
        question: "Which company recently purchased Github?",
        options: [
          {
            text: "Apple",
            validation: false
          },
          {
            text: "Microsoft",
            validation: true
          },
          {
            text: "Joyjet",
            validation: false
          },
          {
            text: "Google",
            validation: false
          }
        ]
      },
      {
        question: "Which of the following technologies is frontend?",
        options: [
          {
            text: "PHP",
            validation: false
          },
          {
            text: "Vue.js",
            validation: true
          },
          {
            text: "C++",
            validation: false
          },
          {
            text: "R",
            validation: false
          }
        ]
      },
      {
        question:
          "The javaScript React library was developed by which of these companies?",
        options: [
          {
            text: "Facebook",
            validation: true
          },
          {
            text: "Mc Donalds",
            validation: false
          },
          {
            text: "Twitter",
            validation: false
          },
          {
            text: "Google",
            validation: false
          }
        ]
      },
      {
        question: "Currently, in which major version is Bootstrap?",
        options: [
          {
            text: "1",
            validation: false
          },
          {
            text: "4",
            validation: true
          },
          {
            text: "15",
            validation: false
          },
          {
            text: "7",
            validation: false
          }
        ]
      },
      {
        question: "Evan You was the creator of the following library...",
        options: [
          {
            text: "jQuery",
            validation: false
          },
          {
            text: "Kendo UI",
            validation: false
          },
          {
            text: "Vue.js",
            validation: true
          },
          {
            text: "Angular",
            validation: false
          }
        ]
      }
    ]
  },
  {
    objectID: 2,
    name: "About the game dota 2",
    content: [
      {
        question: "It is NOT considered a basic attribute ...",
        options: [
          {
            text: "Strength",
            validation: false
          },
          {
            text: "Intelligence",
            validation: false
          },
          {
            text: "Fast",
            validation: true
          },
          {
            text: "Agility",
            validation: false
          }
        ]
      },
      {
        question: "What is the Hero with the most skills?",
        options: [
          {
            text: "Invoker",
            validation: true
          },
          {
            text: "Bane",
            validation: false
          },
          {
            text: "Keeper of the light",
            validation: false
          },
          {
            text: "Storm Spirit",
            validation: false
          }
        ]
      },
      {
        question: "Which professional team has won the last The International?",
        options: [
          {
            text: "PSG.LGD",
            validation: false
          },
          {
            text: "Evil Geniuses",
            validation: false
          },
          {
            text: "Team Liquid",
            validation: false
          },
          {
            text: "OG",
            validation: true
          }
        ]
      },
      {
        question: "Typical strength hero",
        options: [
          {
            text: "Puck",
            validation: false
          },
          {
            text: "Leshrac",
            validation: false
          },
          {
            text: "Io",
            validation: false
          },
          {
            text: "Axe",
            validation: true
          }
        ]
      },
      {
        question:
          "In which of the following countries did the last The International",
        options: [
          {
            text: "Canada",
            validation: true
          },
          {
            text: "EUA",
            validation: false
          },
          {
            text: "Brazil",
            validation: false
          },
          {
            text: "France",
            validation: false
          }
        ]
      }
    ]
  }
];

export default listQuiz;
