import React, { Component } from "react";
import Header from "./../Header";
import Button from "./../Button";

class Home extends Component {
  state = {};
  render() {
    return (
      <div>
        <Header />
        <div className="center">
          <p>
            Lorem impsum dolor lorem impsum impsum dolor lorem impsum impsum
            dolor lorem impsum impsum dolor lorem impsum
          </p>
        </div>
        <Button
          className="button-secondary pure-button"
          link="/quizlist"
          text="See quiz list"
        />
        <Button
          className="button-success pure-button"
          link="/newquiz"
          text="Create a new quiz"
        />
      </div>
    );
  }
}

export default Home;
