import React, { Component } from "react";
import "./index.css";
import logo from "./../../img/logo.svg";

class Header extends Component {
  render() {
    return (
      <div className="header pure-menu pure-menu-horizontal pure-menu-fixed">
        <a href="/">
          <img className="logo" src={logo} alt="logo" />
        </a>
        <h4 className="label">Joyjet Quiz - Test Interview</h4>
      </div>
    );
  }
}

export default Header;
