import React, { Component } from "react";
import Header from "./../Header";

// INITIAL JSON
import listQuiz from "./../../api";

class Quiz extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listQuiz
    };
  }

  render() {
    console.log(this.props.match.params.id);
    console.log(this.state.listQuiz);
    const currentId = this.state.listQuiz.filter(current => {
      return current.objectID.toString() === this.props.match.params.id;
    });
    console.log(currentId);
    return (
      <div>
        <Header />
        <div className="center">
          <h1>{currentId[0].name}</h1>
          <p>{this.state.currentId}</p>
        </div>
      </div>
    );
  }
}

export default Quiz;
