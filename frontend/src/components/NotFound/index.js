import React, { Component } from "react";
import Header from "./../Header";
import Button from "./../Button";

class NotFound extends Component {
  state = {};
  render() {
    return (
      <div className="App">
        <Header />
        <h1>Erro 404</h1>
        <p className="center">Page not found!</p>
        <Button link="/" text="Back to home" />
      </div>
    );
  }
}

export default NotFound;
