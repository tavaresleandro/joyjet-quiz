import React, { Component } from "react";
import { Link } from "react-router-dom";

class Button extends Component {
  constructor(props) {
    super(props);
    this.state = {
      link: this.props.link ? this.props.link : "/",
      text: this.props.text ? this.props.text : "",
      className: this.props.className ? this.props.className : ""
    };
  }
  render() {
    return (
      <div className="center">
        <Link to={this.state.link} className={this.state.className}>
          {this.state.text}
        </Link>
      </div>
    );
  }
}

export default Button;
