import React, { Component } from "react";
import Header from "./../Header";
import Button from "./../Button";

// INITIAL JSON
import listQuiz from "./../../api";

class QuizList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listQuiz
    };
  }

  // Mount name quiz
  getName = (id, name) => {
    return "#" + id + " " + name;
  };

  // Mount link quiz
  getLink = id => {
    return "/quiz/" + id;
  };

  render() {
    console.log(this.state.listQuiz);
    return (
      <div>
        <Header />
        <div className="center">
          <h1>Choose a quiz</h1>
        </div>
        <ul>
          {this.state.listQuiz.map(item => (
            <li key={item.objectID}>
              <Button
                className="button-secondary pure-button"
                link={this.getLink(item.objectID)}
                text={this.getName(item.objectID, item.name)}
              />
            </li>
          ))}
        </ul>
      </div>
    );
  }
}

export default QuizList;
