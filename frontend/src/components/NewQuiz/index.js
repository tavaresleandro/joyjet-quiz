import React, { Component } from "react";
import Header from "./../Header";

class NewQuiz extends Component {
  constructor(props) {
    super(props);

    this.state = {
      objectID: "",
      name: "",
      content: []
    };
  }

  addName = e => {
    let newNameQuiz = this.refs.newNameQuiz.val;
    console.log(newNameQuiz);
    console.log("addNAme");
    e.preventDefault();
  };

  updateVal = e => {
    this.setState({ name: e.target.value });
  };

  render() {
    console.log(this.state);

    return (
      <div>
        <Header />
        <div className="center">
          <h1>New Quiz</h1>
          <form className="pure-form pure-form-stacked">
            <fieldset>
              <div className="pure-g">
                <div className="pure-u-1 pure-u-md-1-3">
                  <label forhtml="first-name">Quiz Name</label>
                  <input
                    ref="newNameQuiz"
                    id="first-name"
                    className="pure-u-23-24"
                    type="text"
                    onChange={this.updateVal}
                    value={this.state.name}
                  />
                  <label forhtml="first-name">Question 1</label>
                  <input
                    ref="newNameQuiz1"
                    id="first-name1"
                    className="pure-u-23-24"
                    type="text"
                    onChange={this.updateVal}
                    value={this.state.name}
                  />
                  <label forhtml="first-name">Question 2</label>
                  <input
                    ref="newNameQuiz1"
                    id="first-name1"
                    className="pure-u-23-24"
                    type="text"
                    onChange={this.updateVal}
                    value={this.state.name}
                  />
                </div>
              </div>
              <button
                onClick={this.addName}
                type="submit"
                className="pure-button pure-button-primary"
              >
                Next
              </button>
              <button
                onClick={this.addName}
                type="submit"
                className="pure-button pure-button-primary button-success"
              >
                Next
              </button>
            </fieldset>
          </form>
        </div>
      </div>
    );
  }
}

export default NewQuiz;
