import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";

// Pages
import Home from "./../Home";
import QuizList from "./../QuizList";
import Quiz from "./../Quiz";
import NewQuiz from "./../NewQuiz";
import NotFound from "./../NotFound";

class Routes extends Component {
  render() {
    return (
      <div className="App">
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/quizlist" component={QuizList} />
          <Route path="/quiz/:id" component={Quiz} />
          <Route path="/newquiz" component={NewQuiz} />
          <Route component={NotFound} />
        </Switch>
      </div>
    );
  }
}

export default Routes;
